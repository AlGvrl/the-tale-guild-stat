jest.unmock('../../../src/client/Guilds/GuildsTable.js');
jest.unmock('../../../src/client/Guilds/GuildTR.js');


import React from 'react';
import TestUtils from 'react-addons-test-utils';
import GuildsTable from '../../../src/client/Guilds/GuildsTable.js';
import GuildTR from '../../../src/client/Guilds/GuildTR.js';

describe('GuildsTable', function() {

    beforeEach(function() {
        this.guilds = [
            {
                name: '[F] Foo',
                totalMight: 111.11,
                playerCount: 11,
                guildID: 1,
            }, {
                name: '[Br] Bar',
                totalMight: 222.22,
                playerCount: 22,
                guildID: 2,
            }, {
                name: '[Bz] Baz',
                totalMight: 333.33,
                playerCount: 33,
                guildID: 3,
            }
        ];
    });

    it('should render', function() {
        const renderer = TestUtils.createRenderer();
        renderer.render(
            <GuildsTable guilds={this.guilds} />
        );
        const result = renderer.getRenderOutput();
        const table = result.props.children;
        const thead = table.props.children[0];
        const tr = thead.props.children;
        const ths = tr.props.children;
        expect(result).toEqual(
            <div>
                <table className="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style={{cursor: 'pointer'}} onClick={ths[1].props.onClick}>
                                ⇕Гильдия
                            </th>
                            <th style={{cursor: 'pointer'}} onClick={ths[2].props.onClick}>
                                ⇕Могущество
                            </th>
                            <th style={{cursor: 'pointer'}} onClick={ths[3].props.onClick}>
                                ⇕Число членов
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <GuildTR tableIndex={1} guild={this.guilds[0]} key={1} />
                        <GuildTR tableIndex={2} guild={this.guilds[1]} key={2} />
                        <GuildTR tableIndex={3} guild={this.guilds[2]} key={3} />
                    </tbody>
                </table>
            </div>
        );
    });
});