jest.unmock('../../../src/client/Guilds/GuildTR.js');

import React from 'react';
import {Link} from 'react-router';
import TestUtils from 'react-addons-test-utils';
import GuildTR from '../../../src/client/Guilds/GuildTR.js';

describe('GuildTR', function() {
    it('should render', function(){
        const guildProp = {
            name: 'Foo',
            totalMight: 1234.5678,
            playerCount: 9,
            guildID: 1,
        };
        let renderer = TestUtils.createRenderer();
        renderer.render(<GuildTR tableIndex={3} guild={guildProp} />);
        let result = renderer.getRenderOutput();
        expect(result).toEqual(
            <tr>
                <td>{3}</td>
                <td><Link to={'/guilds/1'}>{'Foo'}</Link></td>
                <td>{1234.57}</td>
                <td>{9}</td>
            </tr>
        );
    });
});