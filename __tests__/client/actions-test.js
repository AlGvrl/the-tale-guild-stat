jest.unmock('../../src/client/configureStore.js');
jest.unmock('../../src/client/reducers.js');
jest.unmock('../../src/client/actions.js');
jest.unmock('redux');
jest.unmock('fetch-mock');

import configureStore from '../../src/client/configureStore.js';
import {fetchGuildsIfNeeded, 
    fetchGuildInfoIfNeeded, 
    switchPtSorting, 
    togglePtColumn} from '../../src/client/actions.js';
import fetchMock from 'fetch-mock';

jest.useRealTimers();

describe('actions', function() {
    describe('fetchGuildsIfNeeded', function() {
        it('should should save fetched guild into store', function() {
            let store = configureStore();
            let expected = [
                {
                    name: '[НБ] Небожители',
                    creed: 'Аргх!',
                    totalMight: 33839.3166666667,
                    playerCount: 3,
                    guildID: 1,
                }, {
                    name: '[ЗВ] Золотой Век',
                    creed: 'Каждый новый день - еще один шанс сделать наш мир светлее и лучше! Так чего же мы ждем?',
                    totalMight: 6011.526000000001,
                    playerCount: 3,
                    guildID: 3,
                }, {
                    name: '[КБ] Коллегия Бардов',
                    creed: '«Ни дня без строчки!»',
                    totalMight: 3351.252,
                    playerCount: 3,
                    guildID: 7,
                }
            ];
            expected.isFetching = false;

            fetchMock.restore();
            fetchMock.get('/api/guilds/', '[{"name":"[НБ] Небожители","creed":"Аргх!","guildID":1,"playerCount":3,"totalMight":33839.3166666667},{"name":"[ЗВ] Золотой Век","creed":"Каждый новый день - еще один шанс сделать наш мир светлее и лучше! Так чего же мы ждем?","guildID":3,"playerCount":3,"totalMight":6011.526000000001},{"name":"[КБ] Коллегия Бардов","creed":"«Ни дня без строчки!»","guildID":7,"playerCount":3,"totalMight":3351.252}]');
            return store.dispatch(fetchGuildsIfNeeded())
                .then(() => expect(store.getState().guilds).toEqual(expected));
        });
    });

    describe('fetchGuildInfoIfNeeded', function() {
        it('should save fetched guild members into store', done => {
            let store = configureStore();
            let expectedMembers = [
                {
                    playerID:1,
                    isActive:true,
                    playerName:'Tiendil',
                    heroName:'Халлр',
                    race:4,
                    money: 947,
                    level: 77,
                    might: 11708.4506666667,
                    isSubscribed: true
                }, {
                    playerID: 1022,
                    isActive: true,
                    playerName:'DedkovAG',
                    heroName:'Темнослав сын Злободара',
                    race: 0,
                    money: 4380,
                    level: 82,
                    might: 19704.708,
                    isSubscribed: true
                }, {
                    playerID: 209,
                    isActive: false,
                    playerName:'Gizoom',
                    heroName:'Сумарр',
                    race: 4,
                    money: 177,
                    level: 58,
                    might: 2426.158,
                    isSubscribed: false
                }
            ];
            expectedMembers.isFetching = false;

            fetchMock.restore();
            fetchMock.get('/api/guilds/', '[{"name":"[НБ] Небожители","creed":"Аргх!","guildID":1,"playerCount":3,"totalMight":33839.3166666667},{"name":"[ЗВ] Золотой Век","creed":"Каждый новый день - еще один шанс сделать наш мир светлее и лучше! Так чего же мы ждем?","guildID":3,"playerCount":3,"totalMight":6011.526000000001},{"name":"[КБ] Коллегия Бардов","creed":"«Ни дня без строчки!»","guildID":7,"playerCount":3,"totalMight":3351.252}]');

            fetchMock.get('/api/guilds/1', '{"name":"[НБ] Небожители","creed":"Аргх!","guildID":1,"members":[{"playerID":1,"isActive":true,"playerName":"Tiendil","heroName":"Халлр","race":4,"money":947,"level":77,"might":11708.4506666667,"isSubscribed":true},{"playerID":1022,"isActive":true,"playerName":"DedkovAG","heroName":"Темнослав сын Злободара","race":0,"money":4380,"level":82,"might":19704.708,"isSubscribed":true},{"playerID":209,"isActive":false,"playerName":"Gizoom","heroName":"Сумарр","race":4,"money":177,"level":58,"might":2426.158,"isSubscribed":false}],"playerCount":3,"totalMight":36859.950000000004}');

            store.dispatch(fetchGuildsIfNeeded())
                .then(() => store.dispatch(fetchGuildInfoIfNeeded(1)))
                .then(() => {
                    let checked = false;
                    try {
                        for (let guild of store.getState().guilds) {
                            if (guild.guildID === 1) {
                                checked = true;
                                expect(guild.members).toEqual(expectedMembers);
                            }
                        }
                        expect(checked).toEqual(true); 
                    } catch(err) {
                        done.fail(err);
                    }
                    done();
                })
                .catch(err => {console.error(err);});
        });
    });

    describe('switchPtSorting', function() {
        it('should switch to another field', function() {
            let store = configureStore();
            let expected = {
                sortBy: 'might',
                sortOrder: 1,
                displayMask: {
                    playerName: true,
                    heroName: true,
                    race: true,
                    might: true,
                    level: true,
                    money: true,
                    activity: true,
                    subscription: true,
                }
            };

            store.dispatch(switchPtSorting('might'));
            expect(store.getState().settings.pt).toEqual(expected);
        });

        it('should reverse sorting', function() {
            let store = configureStore();
            let expected = {
                sortBy: 'might',
                sortOrder: -1,
                displayMask: {
                    playerName: true,
                    heroName: true,
                    race: true,
                    might: true,
                    level: true,
                    money: true,
                    activity: true,
                    subscription: true,
                }
            };

            store.dispatch(switchPtSorting('might'));//switch to 'might'
            store.dispatch(switchPtSorting('might'));//reverse sorting
            expect(store.getState().settings.pt).toEqual(expected);
        });

        it('should restore original sorting direction', function() {
            let store = configureStore();
            let expected = {
                sortBy: 'heroName',
                sortOrder: 1,
                displayMask: {
                    playerName: true,
                    heroName: true,
                    race: true,
                    might: true,
                    level: true,
                    money: true,
                    activity: true,
                    subscription: true,
                }
            };

            store.dispatch(switchPtSorting('might'));//switch
            store.dispatch(switchPtSorting('might'));//reverse
            store.dispatch(switchPtSorting('heroName'));//switch back
            expect(store.getState().settings.pt).toEqual(expected);
        });
    });

    describe('togglePtColumn', function() {
        it('should enable/disable fields in displayMask', function() {
            let store = configureStore();
            let expected = {
                playerName: true,
                heroName: true,
                race: true,
                might: true,
                level: true,
                money: false,
                activity: false,
                subscription: false,
            };

            store.dispatch(togglePtColumn('race'));
            store.dispatch(togglePtColumn('money'));
            store.dispatch(togglePtColumn('activity'));
            store.dispatch(togglePtColumn('subscription'));
            store.dispatch(togglePtColumn('race'));

            expect(store.getState().settings.pt.displayMask).toEqual(expected);
        });
    });
});