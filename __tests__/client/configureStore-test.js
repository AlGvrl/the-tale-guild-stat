jest.unmock('../../src/client/configureStore.js');
jest.unmock('../../src/client/reducers.js');
jest.unmock('redux');

import configureStore from '../../src/client/configureStore.js';

describe('store', function() {
    it('should fill in default values', function() {
        const expected = {
            guilds: [],
            settings: {
                pt: {
                    sortBy: 'heroName',
                    sortOrder: 1,
                    displayMask: {
                        playerName: true,
                        heroName: true,
                        race: true,
                        might: true,
                        level: true,
                        money: true,
                        activity: true,
                        subscription: true,
                    }
                }
            }
        };
        expected.guilds.isFetching = false;

        const store = configureStore();
        const actual = store.getState();
        expect(actual).toEqual(expected);
    });
});