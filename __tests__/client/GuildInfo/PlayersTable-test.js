jest.unmock('../../../src/client/GuildInfo/PlayersTable.js');
jest.unmock('../../../src/client/GuildInfo/PlayersTableSettings.js');
jest.unmock('../../../src/client/GuildInfo/PlayerTR.js');
jest.unmock('react-bootstrap');
jest.unmock('enzyme');

import React from 'react';
import {shallow} from 'enzyme';
import TestUtils from 'react-addons-test-utils';
import PlayersTable from '../../../src/client/GuildInfo/PlayersTable.js';
import PlayersTableSettings from '../../../src/client/GuildInfo/PlayersTableSettings.js';
import PlayerTR from '../../../src/client/GuildInfo/PlayerTR.js';

function setup() {
    const props = {
        players: [
            {
                playerID: 1,
                playerName: 'AFoo',
                heroName: 'Oof',
                race: 0,
                might: 100,
                level: 10,
                money: 1000,
                isActive: false,
                isSubscribed: false,
            }, {
                playerID: 2,
                playerName: 'BBar',
                heroName: 'Rab',
                race: 1,
                might: 200,
                level: 20,
                money: 2000,
                isActive: true,
                isSubscribed: false,
            }, {
                playerID: 3,
                playerName: 'CBaz',
                heroName: 'Zab',
                race: 2,
                might: 300,
                level: 30,
                money: 3000,
                isActive: true,
                isSubscribed: true,
            },
        ],
        displayMask: {
            playerName: true,
            heroName: true,
            race: true,
            might: true,
            level: true,
            money: true,
            activity: true,
            subscription: true,
        },
        onSettingsClick: jest.fn(),
        onHeaderClick: jest.fn()
    };
    const enzymeWrapper = shallow(<PlayersTable {...props} />);
    return {
        props: props,
        eWr: enzymeWrapper,
    };
}

describe('PlayersTable', function() {
    it('should render', function() {
        const {eWr, props} = setup();
        const expHeaders = ['#', '⇕Хранитель', '⇕Герой', '⇕Раса', 
            '⇕Могущество', '⇕Уровень', '⇕Золото', '⇕Активен?', '⇕Подписан?'];
        const headers = eWr.find('thead > tr > th');
        headers.forEach((header, idx) => {
            expect(header.text()).toEqual(expHeaders[idx]);
        });
        const rows = eWr.find('PlayerTR');
        rows.forEach((row, idx) => {
            expect(row.prop('player')).toEqual(props.players[idx]);
        });
        expect(eWr.find('PlayersTableSettings').props()).toEqual({
            displayMask: props.displayMask,
            onSettingsClick: props.onSettingsClick
        });
    });

    it('should pass name of the field to sort by when header is clicked', function() {
        const {eWr, props} = setup();
        const headers = eWr.find('thead > tr > th');
        headers.forEach(header => header.simulate('click'));
        const expectedCalls = [['playerName'], ['heroName'], ['race'], ['might'],
            ['level'], ['money'], ['isActive'], ['isSubscribed']];
        expect(props.onHeaderClick.mock.calls).toEqual(expectedCalls);
    });
});