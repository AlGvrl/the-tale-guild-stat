jest.unmock('../../../src/client/GuildInfo/PlayersTableSettings.js');
jest.unmock('react-bootstrap');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import PlayersTableSettings from '../../../src/client/GuildInfo/PlayersTableSettings.js';
import {DropdownButton} from 'react-bootstrap';

describe('PlayersTableSettings', function() {
    beforeEach(function() {
        this.clickHandlerMock = jest.fn();
        this.clickHandler = this.clickHandlerMock.bind(this);
        this.maskProp = {
            playerName: false,
            heroName: true,
            race: true,
            might: true,
            level: true,
            money: false,
            activity: true,
            subscription: true,
        };
        this.renderer = TestUtils.createRenderer();
    });

    it('should render', function() {
        const stats = ['playerName', 'heroName', 'race', 'might', 'level', 'money', 'activity', 'subscription'];
        this.renderer.render(<PlayersTableSettings
            displayMask={this.maskProp}
            onSettingsClick={this.clickHandler}
        />);
        let result = this.renderer.getRenderOutput();
        expect(result.type).toEqual(DropdownButton);
        result.props.children.forEach((child, idx) => {
            expect(child.type).toEqual('div');
            expect(child.props.className).toEqual('checkbox');
            let label = child.props.children;
            expect(label.type).toEqual('label');
            let input = label.props.children[0];
            expect(input.type).toEqual('input');
            expect(input.props.type).toEqual('checkbox');
            expect(input.props.checked).toEqual(this.maskProp[stats[idx]]);
        });
        
    });

    it('should bind right params to the onChange handlers', function() {
        const expected = [['playerName'], ['heroName'], ['race'], ['might'], ['level'], ['money'], ['activity'], ['subscription']];
        this.renderer.render(<PlayersTableSettings
            displayMask={this.maskProp}
            onSettingsClick={this.clickHandler}
        />);
        let result = this.renderer.getRenderOutput();
        result.props.children.forEach((child) => {
            let input = child.props.children.props.children[0];
            input.props.onChange.bind('boo')();
        });
        expect(this.clickHandlerMock.mock.calls).toEqual(expected);
    });
});