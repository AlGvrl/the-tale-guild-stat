jest.unmock('../../../src/client/GuildInfo/GuildSummary.js');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import GuildSummary from '../../../src/client/GuildInfo/GuildSummary.js';

describe('GuildSummary', function() {

    beforeEach(function() {
        this.props = {
            totalMight: 10000.1234,
            playerCount: 11,
            racialDistribution: [3, 2, 1, 3, 2],
            levelInfo: {
                minLevel: 10,
                maxLevel: 50,
                meanLevel: 25,
            },
        };
    });

    it('should render', function() {
        const renderer = TestUtils.createRenderer();
        renderer.render(<GuildSummary {...this.props} />);
        const result = renderer.getRenderOutput();
        expect(result).toEqual(
            <div>
                <h4>Вкратце</h4>
                <ul>
                    <li>Могущество
                        <ul>
                            <li>
                                Общее
                                <br />
                                {10000.12}
                            </li>
                            <li>
                                Среднее
                                <br />
                                {909.1}
                            </li>
                        </ul>
                    </li>

                    <li>
                        Расы
                        <ul>
                            <li key={0}>
                                {'Люди'} - {3} ({27}%)
                            </li>
                            <li key={1}>
                                {'Эльфы'} - {2} ({18}%)
                            </li>
                            <li key={2}>
                                {'Орки'} - {1} ({9}%)
                            </li>
                            <li key={3}>
                                {'Гоблины'} - {3} ({27}%)
                            </li>
                            <li key={4}>
                                {'Дварфы'} - {2} ({18}%)
                            </li>
                        </ul>
                    </li>


                    <li>
                        Уровень
                        <ul>
                            <li>
                                Максимальный
                                <br />
                                {50}
                            </li>
                            <li>
                                Минимальный
                                <br />
                                {10}
                            </li>
                            <li>
                                Средний
                                <br />
                                {25}
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        );
    });

});