jest.unmock('../../../src/client/GuildInfo/PlayerTR.js');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import PlayerTR from '../../../src/client/GuildInfo/PlayerTR.js';

describe('PlayerTR', function() {
    beforeEach(function() {
        this.playerProp = {
            playerName: 'Foo',
            heroName: 'Bar',
            race: 0,
            might: 300.1234,
            level: 11,
            money: 3000,
            isActive: true,
            isSubscribed: false,
        };
        this.maskProp = {
            playerName: true,
            heroName: true,
            race: true,
            might: true,
            level: true,
            money: true,
            activity: true,
            subscription: true,
        };
        this.renderer = TestUtils.createRenderer();
    });

    it('should render all the player info', function() {
        this.renderer.render(<PlayerTR 
            tableIndex={10} 
            player={this.playerProp} 
            displayMask={this.maskProp} 
        />);
        let result = this.renderer.getRenderOutput();

        expect(result.type).toEqual('tr');
        expect(result.props.children).toEqual([
            <td>{10}</td>,
            <td>Foo</td>,
            <td>Bar</td>,
            <td>Человек</td>,
            <td>{300.12}</td>,
            <td>{11}</td>,
            <td>{3000}</td>,
            <td>Да</td>,
            <td>Нет</td>,
        ]);
    });

    it('should omit some columns according to the display mask', function() {
        this.maskProp.playerName = false;
        this.maskProp.subscription = false;
        this.renderer.render(<PlayerTR 
            tableIndex={10} 
            player={this.playerProp} 
            displayMask={this.maskProp} 
        />);
        let result = this.renderer.getRenderOutput();
        expect(result.props.children).toEqual([
            <td>{10}</td>,
            null,
            <td>Bar</td>,
            <td>Человек</td>,
            <td>{300.12}</td>,
            <td>{11}</td>,
            <td>{3000}</td>,
            <td>Да</td>,
            null,
        ]);
    });

    it('should be able to omit all columns', function() {
        this.maskProp = {
            playerName: false,
            heroName: false,
            race: false,
            might: false,
            level: false,
            money: false,
            activity: false,
            subscription: false,
        };

        this.renderer.render(<PlayerTR 
            tableIndex={10} 
            player={this.playerProp} 
            displayMask={this.maskProp} 
        />);
        let result = this.renderer.getRenderOutput();
        expect(result.props.children).toEqual([
            <td>{10}</td>,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]);
    });
});