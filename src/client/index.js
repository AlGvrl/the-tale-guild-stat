import React from 'react';
import {render} from 'react-dom';
import {Router, Route, Redirect, useRouterHistory} from 'react-router';
import {createHashHistory} from 'history';
import {Provider} from 'react-redux';
import GuildsWrapper from './Guilds/GuildsWrapper.js';
import GuildInfoWrapper from './GuildInfo/GuildInfoWrapper.js';
import configureStore from './configureStore.js';

function App(props) {
    return (
        <div>
            {props.children}
        </div>
    );
}

App.propTypes = {
    children: React.PropTypes.element.isRequired
};

const store = configureStore();

const appHistory = useRouterHistory(createHashHistory)({ queryKey: false });
render((
    <Provider store={store}>
        <Router history={appHistory}>
            <Redirect from="/" to="/guilds" />
            <Route path="/" component={App}>
                <Route path="guilds/:id" component={GuildInfoWrapper} />
                <Route path="guilds" component={GuildsWrapper} />
            </Route>
        </Router>
    </Provider>
), document.getElementById('root'));