import 'isomorphic-fetch';

//Maybe implement invalidation to refresh data?

export const actions = {
    REQUEST_GUILDS: 'REQUEST_GUILDS',
    RECEIVE_GUILDS: 'RECEIVE_GUILDS',
    REQUEST_GUILD_INFO: 'REQUEST_GUILD_INFO',
    RECEIVE_GUILD_INFO: 'RECEIVE_GUILD_INFO',
    TOGGLE_PT_COLUMN: 'TOGGLE_PT_COLUMN',
    SWITCH_PT_SORTING: 'SWITCH_PT_SORTING',
};

function requestGuilds() {
    return {
        type: actions.REQUEST_GUILDS
    };
}

function receiveGuilds(data) {
    return {
        type: actions.RECEIVE_GUILDS,
        data
    };
}

function shouldFetchGuilds(guilds) {
    return !guilds.isFetching;
}

function fetchGuilds() {
    return dispatch => {
        dispatch(requestGuilds());
        return fetch('/api/guilds/')
            .then(res => res.json())
            .then(data => dispatch(receiveGuilds(data)));
    };
}

export function fetchGuildsIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchGuilds(getState().guilds)) {
            return dispatch(fetchGuilds());
        } else {
            return Promise.resolve();
        }
    };
}

function requestGuildInfo(guildID) {
    return {
        type: actions.REQUEST_GUILD_INFO,
        guildID
    };
}

function receiveGuildInfo(guildID, data) {
    return {
        type: actions.RECEIVE_GUILD_INFO,
        guildID,
        data
    };
}

function shouldFetchGuildInfo(guild) {
    if (guild == null) {
        return true;
    }
    if (guild.members == null || (guild.members.length === 0 && !guild.members.isFetching)) {
        return true;
    }
    return false;
}

function fetchGuildInfo(guildID) {
    return dispatch => {
        dispatch(requestGuildInfo(guildID));
        return fetch('/api/guilds/' + guildID)
            .then(res => res.json())
            .then((data) => {
                return dispatch(receiveGuildInfo(guildID, data));
            });
    };
}

export function fetchGuildInfoIfNeeded(guildID) {
    return (dispatch, getState) => {
        if (shouldFetchGuildInfo(getState().guilds.find(x => x.guildID === guildID))) {
            return dispatch(fetchGuildInfo(guildID));
        } else {
            return Promise.resolve();
        }
    };
}

export function togglePtColumn(column) {
    return {
        type: actions.TOGGLE_PT_COLUMN,
        column
    };
}

export function switchPtSorting(column) {
    return {
        type: actions.SWITCH_PT_SORTING,
        column
    };
}