import React from 'react';

export default function PlayerTR({tableIndex, displayMask, player}) {
    const races = ['Человек', 'Эльф', 'Орк', 'Гоблин', 'Дварф'];
    return ( 
    <tr>
        <td>{tableIndex}</td>
        {displayMask.playerName ? 
            <td>{player.playerName}</td> : 
            null
        }
        {displayMask.heroName ? 
            <td>{player.heroName}</td> : 
            null
        }
        {displayMask.race ? 
            <td>{races[player.race]}</td> : 
            null
        }
        {displayMask.might ? 
            <td>{+(player.might.toFixed(2))}</td> : 
            null
        }
        {displayMask.level ? 
            <td>{player.level}</td> : 
            null
        }
        {displayMask.money ? 
            <td>{player.money}</td> : 
            null
        }
        {displayMask.activity ? 
            <td>{player.isActive? 'Да' : 'Нет'}</td> : 
            null
        }
        {displayMask.subscription ? 
            <td>{player.isSubscribed? 'Да' : 'Нет'}</td> : 
            null
        }
    </tr>
    );
}

PlayerTR.propTypes = {
    tableIndex: React.PropTypes.number.isRequired,
    displayMask: React.PropTypes.shape({
        playerName: React.PropTypes.bool.isRequired,
        heroName: React.PropTypes.bool.isRequired,
        race: React.PropTypes.bool.isRequired,
        might: React.PropTypes.bool.isRequired,
        level: React.PropTypes.bool.isRequired,
        money: React.PropTypes.bool.isRequired,
        activity: React.PropTypes.bool.isRequired,
        subscription: React.PropTypes.bool.isRequired,
    }).isRequired,
    player: React.PropTypes.shape({
        playerName: React.PropTypes.string.isRequired,
        heroName: React.PropTypes.string.isRequired,
        race: React.PropTypes.number.isRequired,
        might: React.PropTypes.number.isRequired,
        level: React.PropTypes.number.isRequired,
        money: React.PropTypes.number.isRequired,
        isActive: React.PropTypes.bool.isRequired,
        isSubscribed: React.PropTypes.bool.isRequired,
    }).isRequired,
};