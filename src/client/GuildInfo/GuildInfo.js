import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import GuildSummary from './GuildSummary.js';
import SortablePlayersTable from './SortablePlayersTable.js';

export default class GuildInfo extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.didMount(this.props.guildID);
    }

    render() {
        let guild = this.props.guild;
        if (guild != null) {
            let racialDistribution = [0, 0, 0, 0, 0];
            let levelInfo = {
                minLevel: 1000,
                maxLevel: 1,
                meanLevel: 0,
            };
            if (guild.members) {
                for (let member of guild.members) {
                    racialDistribution[member.race]++;
                    levelInfo.minLevel = Math.min(levelInfo.minLevel, member.level);
                    levelInfo.maxLevel = Math.max(levelInfo.maxLevel, member.level);
                    levelInfo.meanLevel += member.level / guild.playerCount;
                }
            }
            return (
                <Grid fluid>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={10} lgOffset={1}>
                            <h1>
                                {guild.name}
                                <br/>
                                <small>
                                    {guild.creed}
                                </small>
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} sm={4} md={4} lg={3} lgOffset={1}>
                            <GuildSummary
                                totalMight={guild.totalMight}
                                playerCount={guild.playerCount}
                                racialDistribution={racialDistribution}
                                levelInfo={levelInfo}
                            />
                        </Col>
                        <Col xs={12} sm={8} md={8} lg={7} lgPull={1}>
                            <SortablePlayersTable
                                guildID={this.props.guildID}
                            />
                        </Col>
                    </Row>
                </Grid>
            );
        } else {
            return (
                <div>
                </div>
            );
        }
    }
}

GuildInfo.propTypes = {
    guildID: React.PropTypes.number.isRequired,
    guild: React.PropTypes.shape({
        name: React.PropTypes.string.isRequired,
        creed: React.PropTypes.string.isRequired,
        totalMight: React.PropTypes.number.isRequired,
        playerCount: React.PropTypes.number,
        guildID: React.PropTypes.number.isRequired,
        members: React.PropTypes.arrayOf(React.PropTypes.object),
    }),
    didMount: React.PropTypes.func.isRequired,
};