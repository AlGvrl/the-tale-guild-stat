import React from 'react';
import PlayerTR from './PlayerTR.js';
import PlayersTableSettings from './PlayersTableSettings.js';


export default function PlayersTable ({players, displayMask, onSettingsClick, onHeaderClick}) {
    return (
        <div>
            <PlayersTableSettings
                displayMask={displayMask}
                onSettingsClick={onSettingsClick}
            />
            <table className="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        {displayMask.playerName ? 
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('playerName')}>
                                ⇕Хранитель
                            </th> :
                            null
                        }
                        {displayMask.heroName ?
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('heroName')}>
                                ⇕Герой
                            </th> :
                            null
                        }
                        {displayMask.race ?
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('race')}>
                                ⇕Раса
                            </th> :
                            null 
                        }
                        {displayMask.might ?
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('might')}>
                                ⇕Могущество
                            </th> :
                            null
                        }
                        {displayMask.level ? 
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('level')}>
                                ⇕Уровень
                            </th> :
                            null
                        }
                        {displayMask.money ? 
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('money')}>
                                ⇕Золото
                            </th> :
                            null
                        }
                        {displayMask.activity ?
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('isActive')}>
                                ⇕Активен?
                            </th> :
                            null
                        }
                        {displayMask.subscription ?
                            <th style={{cursor: 'pointer'}} onClick={() => onHeaderClick('isSubscribed')}>
                                ⇕Подписан?
                            </th> :
                            null
                        }
                    </tr>
                </thead>
                <tbody>
                    {players ?
                        players.map(
                            (player, idx) => <PlayerTR 
                                key={player.playerID}
                                player={player}
                                tableIndex={idx + 1}
                                displayMask={displayMask}
                            /> 
                        )
                    :
                        null
                    }
                </tbody>
            </table>
        </div>
    );
}

PlayersTable.propTypes = {
    players: React.PropTypes.arrayOf(React.PropTypes.shape({
        playerID: React.PropTypes.number.isRequired,
        playerName: React.PropTypes.string.isRequired,
        heroName: React.PropTypes.string.isRequired,
        race: React.PropTypes.number.isRequired,
        might: React.PropTypes.number.isRequired,
        level: React.PropTypes.number.isRequired,
        money: React.PropTypes.number.isRequired,
        isActive: React.PropTypes.bool.isRequired,
        isSubscribed: React.PropTypes.bool.isRequired,
    })),
    displayMask: React.PropTypes.shape({
        playerName: React.PropTypes.bool.isRequired,
        heroName: React.PropTypes.bool.isRequired,
        race: React.PropTypes.bool.isRequired,
        might: React.PropTypes.bool.isRequired,
        level: React.PropTypes.bool.isRequired,
        money: React.PropTypes.bool.isRequired,
        activity: React.PropTypes.bool.isRequired,
        subscription: React.PropTypes.bool.isRequired,
    }).isRequired,
    onSettingsClick: React.PropTypes.func.isRequired,
    onHeaderClick: React.PropTypes.func.isRequired,
};