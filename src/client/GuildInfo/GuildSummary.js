import React from 'react';

export default function GuildSummary({totalMight, playerCount, racialDistribution, levelInfo}) {
    const races = ['Люди','Эльфы','Орки','Гоблины','Дварфы'];

    return (
        <div>
            <h4>Вкратце</h4>
            <ul>

                <li>
                    Могущество
                    <ul>
                        <li>
                            Общее
                            <br/>
                            {+(totalMight).toFixed(2)}
                        </li>
                        <li>
                            Среднее
                            <br/>
                            {+(totalMight / playerCount).toFixed(2)}
                        </li>
                    </ul>
                </li>

                <li>
                    Расы
                    <ul>
                        {racialDistribution.map(
                            (raceCount, idx) => 
                                <li key={idx}>
                                    {races[idx]} - {raceCount} ({Math.round(100 * Number((raceCount / playerCount).toFixed(2)))}%)
                                </li>
                        )}
                    </ul>
                </li>

                <li>
                    Уровень
                    <ul>
                        <li>
                            Максимальный
                            <br/>
                            {levelInfo.maxLevel}
                        </li>
                        <li>
                            Минимальный
                            <br/>
                            {levelInfo.minLevel}
                        </li>
                        <li>
                            Средний
                            <br/>
                            {+levelInfo.meanLevel.toFixed(2)}
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    );
}

GuildSummary.propTypes = {
    totalMight: React.PropTypes.number.isRequired,
    playerCount: React.PropTypes.number.isRequired,
    racialDistribution: React.PropTypes.arrayOf(React.PropTypes.number).isRequired,
    levelInfo: React.PropTypes.shape({
        minLevel: React.PropTypes.number.isRequired,
        maxLevel: React.PropTypes.number.isRequired,
        meanLevel: React.PropTypes.number.isRequired
    }).isRequired
};