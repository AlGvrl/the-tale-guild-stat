import React from 'react';
import {DropdownButton} from 'react-bootstrap';

export default function PlayersTableSettings({displayMask, onSettingsClick}) {
    return (
        <DropdownButton title={"⚙  Настройки"}>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('playerName')}
                        checked={displayMask.playerName}
                    />
                    Хранитель
                </label>
            </div>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('heroName')}
                        checked={displayMask.heroName}
                    />
                    Герой
                </label>
            </div>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('race')}
                        checked={displayMask.race}
                    />
                    Раса
                </label>
            </div>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('might')}
                        checked={displayMask.might}
                    />
                    Могущество
                </label>
            </div>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('level')}
                        checked={displayMask.level}
                    />
                    Уровень
                </label>
            </div>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('money')}
                        checked={displayMask.money}
                    />
                    Золото
                </label>
            </div>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('activity')}
                        checked={displayMask.activity}
                    />
                    Активность
                </label>
            </div>
            <div className="checkbox">
                <label style={{marginLeft: '1.5rem'}}>
                    <input 
                        type="checkbox" 
                        onChange={() => onSettingsClick('subscription')}
                        checked={displayMask.subscription}
                    />
                    Подписка
                </label>
            </div>
        </DropdownButton>
    );
}

PlayersTableSettings.propTypes = {
    displayMask: React.PropTypes.shape({
        playerName: React.PropTypes.bool.isRequired,
        heroName: React.PropTypes.bool.isRequired,
        race: React.PropTypes.bool.isRequired,
        might: React.PropTypes.bool.isRequired,
        level: React.PropTypes.bool.isRequired,
        money: React.PropTypes.bool.isRequired,
        activity: React.PropTypes.bool.isRequired,
        subscription: React.PropTypes.bool.isRequired,
    }).isRequired,
    onSettingsClick: React.PropTypes.func.isRequired
};