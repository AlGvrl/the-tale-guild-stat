//acts as a container component for PlayersTable

import {connect} from 'react-redux';
import {PropTypes} from 'react';
import PlayersTable from './PlayersTable.js';
import {togglePtColumn, switchPtSorting} from '../actions.js';

function getSortedMembers(guilds, guildID, ptSettings) {
    const {sortBy, sortOrder} = ptSettings;
    const guild = guilds.find(elem => elem.guildID === guildID);
    if (guild == null || guild.members == null) {
        return [];
    }
    const members = guild.members.slice();
    members.sort((a, b) => sortOrder * (a[sortBy] < b[sortBy] ? -1 : 1));
    return members;
}

function mapStateToProps(state, ownProps) {
    return {
        players: getSortedMembers(state.guilds, ownProps.guildID, state.settings.pt),
        displayMask: state.settings.pt.displayMask,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onSettingsClick: (setting) => {
            dispatch(togglePtColumn(setting));
        },
        onHeaderClick: (header) => {
            dispatch(switchPtSorting(header));
        },
    };
}

const SortablePlayersTable = connect(
    mapStateToProps,
    mapDispatchToProps
)(PlayersTable);

//not sure if this works as expected
SortablePlayersTable.propTypes = {
    guildID: PropTypes.number.isRequired,
};

export default SortablePlayersTable;