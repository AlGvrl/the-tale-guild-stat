import {connect} from 'react-redux';
import {PropTypes} from 'react';
import GuildInfo from './GuildInfo.js';
import {fetchGuildInfoIfNeeded} from '../actions.js';

function mapStateToProps(state, ownProps) {
    let guild = state.guilds.find(elem => elem.guildID === Number(ownProps.params.id));
    if (guild == null) {
        //TODO: redirect? 404?
    }
    return {
        guild,
        guildID: Number(ownProps.params.id),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        didMount: (guildID) => {
            return dispatch(fetchGuildInfoIfNeeded(guildID));
        }
    };
}

const GuildInfoWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(GuildInfo);

GuildInfoWrapper.propTypes = {
    params: PropTypes.shape({
        id: PropTypes.string.isRequired,
    })
};
export default GuildInfoWrapper;