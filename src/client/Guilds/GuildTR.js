import React from 'react';
import {Link} from 'react-router';

export default function GuildTR({tableIndex, guild}) {
    return (
        <tr>
            <td>{tableIndex}</td>
            <td><Link to={'/guilds/' + guild.guildID}>{guild.name}</Link></td>
            <td>{+guild.totalMight.toFixed(2)}</td>
            <td>{guild.playerCount}</td>
        </tr>
    );
}

GuildTR.propTypes = {
    tableIndex: React.PropTypes.number.isRequired,
    guild: React.PropTypes.shape({
        name: React.PropTypes.string.isRequired,
        totalMight: React.PropTypes.number.isRequired,
        playerCount: React.PropTypes.number.isRequired,
        guildID: React.PropTypes.number.isRequired,
    }).isRequired,
};