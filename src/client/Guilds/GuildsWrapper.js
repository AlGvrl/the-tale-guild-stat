import {connect} from 'react-redux';
import Guilds from './Guilds.js';
import {fetchGuildsIfNeeded} from '../actions.js';

function mapStateToProps(state) {
    return {
        guilds: state.guilds,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        didMount: () => {
            return dispatch(fetchGuildsIfNeeded());
        }
    };
}

const GuildsWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(Guilds);

export default GuildsWrapper;