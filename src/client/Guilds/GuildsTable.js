import React from 'react';
import GuildTR from './GuildTR.js';

export default class GuildsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sortField: 'guildID',
            sortOrder: 1,
        };

        this.handleHeaderClick = this.handleHeaderClick.bind(this);
    }

    handleHeaderClick(header) {
        let newState = {};
        if (header === this.state.sortField) {
            Object.assign(newState, this.state, {sortOrder: -1 * this.state.sortOrder});
        } else {
            Object.assign(newState, this.state, {sortField: header, sortOrder: 1});
        }
        this.setState(newState);
    }

    render() {
        let sortedGuilds = this.props.guilds.slice();
        sortedGuilds.forEach(function(guild) {
            guild.trimmedName = guild.name.match(/\[.*\] (.*)/)[1];
        });
        sortedGuilds.sort((a, b) => this.state.sortOrder * (a[this.state.sortField] < b[this.state.sortField]? -1 : 1));
        return (
            <div>
                <table className="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style={{cursor: 'pointer'}} onClick={this.handleHeaderClick.bind(null, 'trimmedName')}>
                                ⇕Гильдия
                            </th>
                            <th style={{cursor: 'pointer'}} onClick={this.handleHeaderClick.bind(null, 'totalMight')}>
                                ⇕Могущество
                            </th>
                            <th style={{cursor: 'pointer'}} onClick={this.handleHeaderClick.bind(null, 'playerCount')}>
                                ⇕Число членов
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {sortedGuilds.map(
                            (guild, idx) => <GuildTR
                                key={guild.guildID}
                                tableIndex={idx + 1}
                                guild={guild} 
                            />
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}

GuildsTable.propTypes = {
    guilds: React.PropTypes.arrayOf(React.PropTypes.shape({
        name: React.PropTypes.string.isRequired,
        totalMight: React.PropTypes.number.isRequired,
        playerCount: React.PropTypes.number.isRequired,
        guildID: React.PropTypes.number.isRequired,
    })).isRequired,
};