import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers.js';

export default function configureStore() {
    const store = createStore(
        rootReducer,
        applyMiddleware(thunk)
    );
    //kickstart it to fill in default values
    store.dispatch({type: ''});
    return store;
}