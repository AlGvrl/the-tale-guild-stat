import {combineReducers} from 'redux';

function guildsReducer(guilds, action) {
    if (guilds == null) {
        guilds = [];
        guilds.isFetching = false;
    }

    switch (action.type) {
        case 'REQUEST_GUILDS':
            return Object.assign([], guilds, {isFetching: true});
        case 'RECEIVE_GUILDS':
            return Object.assign(action.data, {isFetching: false});
        case 'REQUEST_GUILD_INFO':
            return guilds.map(guild => {
                if (guild.guildID === action.guildID) {
                    return Object.assign(
                        {},
                        guild,
                        {members: Object.assign([], guild.members, {isFetching: true})}
                    );
                }
                return guild;
            });
        case 'RECEIVE_GUILD_INFO':
            var inserted = false;
            var result = guilds.map(guild => {
                if (guild.guildID === action.guildID) {
                    inserted = true;
                    return Object.assign(
                        {},
                        guild,
                        {members: Object.assign(action.data.members, {isFetching: false})}
                    );
                }
                return guild;
            });
            if (!inserted && action.data != null) {
                return result.concat(action.data);
            } else {
                return result;
            }
        default:
            return guilds;
    }
}

function settingsPtReducer(state, action) {
    if (state == null) {
        state = {
            sortBy: 'heroName',
            sortOrder: 1,
            displayMask: {
                playerName: true,
                heroName: true,
                race: true,
                might: true,
                level: true,
                money: true,
                activity: true,
                subscription: true,
            }
        };
    }

    const newState = Object.assign({}, state);
    switch (action.type) {
        case 'TOGGLE_PT_COLUMN':
            newState.displayMask[action.column] = !state.displayMask[action.column];
            return newState;
        case 'SWITCH_PT_SORTING':
            if (action.column === state.sortBy) {
                newState.sortOrder *= -1;
            } else {
                Object.assign(newState, {sortBy: action.column, sortOrder: 1});
            }
            return newState;
        default:
            return state;
    }
}

const settingsReducer = combineReducers({
    pt: settingsPtReducer
});

const rootReducer = combineReducers({
    guilds: guildsReducer,
    settings: settingsReducer
});

export default rootReducer;