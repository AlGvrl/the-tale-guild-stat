const cfg = require('./cfg.js');
const promisify = require('es6-promisify');
const request = promisify(require('request'), {multiArgs: true});
const getDb = require('./db.js');
const error = require('debug')('updater');
const cheerio = require('cheerio');
const entities = require('entities');

export function checkGuildForUpdates(guildID) {
    guildID = Number(guildID);
    let db = null;
    let guildsCol = null;
    return getDb().then(result => {
        db = result;
        guildsCol = db.collection('guilds');
        return guildsCol.findOne({'guildID': guildID});
    }).then(guild => {
        if (!guild || (Date.now() - guild.lastUpdated > cfg.minUpdateTime)) {
            return updateGuild(guildID);
        } else {
            return;
        }
    }).catch(err => {
        error(err);
    });
}

export function updateAllGuilds() {
    let i = 0;
    const concurrency = 3;
    let IDs = [];
    while (i++ < cfg.maxGuildID) {
        IDs.push(i);
    }
    return promiseMap(IDs, ID => updateGuild(ID), concurrency)
    .catch(err => {
        error(err);
    });
}

export function checkExistingGuildsForUpdates() {
    let db = null;
    let guildsCol = null;
    return getDb().then(result => {
        db = result;
        guildsCol = db.collection('guilds');
        return guildsCol.find({}).toArray();
    }).then(guilds => {
        const concurrency = 3;
        return promiseMap(guilds, guild => checkGuildForUpdates(guild.guildID), concurrency);
    }).catch(err => {
        error(err);
    });
}

export function updateGuild(guildID) {
    return request('http://the-tale.org/accounts/clans/' + guildID).then(function([ , body]) {
        if (/неверный идентификатор гильдии/gi.test(body)) {
            throw new Error('Guild does not exist');
        }
        let guild = parseGuildInfo(body);
        guild.guildID = guildID;
        let members = parseGuildMembers(body);
        const concurrency = 5;
        let membersInfo = promiseMap(members, m => fetchPlayerInfo(m.playerID), concurrency);
        return Promise.all([guild, members, membersInfo]);
    }).then(function([guild, membersFromGuildPage, membersFromApi]) {
        if (membersFromGuildPage.length != membersFromApi.length) {
            throw new Error('Unreachable');
        }
        guild.members = [];
        membersFromGuildPage.forEach((val, idx) => {
            guild.members.push(Object.assign({}, val, membersFromApi[idx]));
        });
        guild.playerCount = guild.members.length;
        guild.totalMight = 0;
        guild.members.forEach(m => {
            guild.totalMight += m.might;
        });
        return Promise.all([guild, getDb()]);
    }).then(function([guild, db]) {
        const guildsCol = db.collection('guilds');
        guildsCol.updateOne({guildID: guild.guildID}, guild, {upsert: true});
    }).catch(err => {
        if (err.message !== 'Guild does not exist') {
            error(err);
        }
    });
}

function parseGuildInfo(guildPageBody) {
    const $ = cheerio.load(guildPageBody, {decodeEntities: false});
    let guild = {};
    guild.name = $('h2').html().match(/\[[^<]*/)[0].replace('\n', '').trim().replace('  ', ' ');
    guild.name = entities.decodeHTML(guild.name);
    guild.creed = entities.decodeHTML($('h2 small').html());
    guild.lastUpdated = new Date();
    return guild;
}

function parseGuildMembers(guildPageBody) {
    const regex = /<td>\s*<a href=\"\/accounts\/(\d*)\"\s*class=\"(.*)\">\s*(.*)\s*<\/a>\s*<\/td>/gi;// \1--account id; \2--isDisabled; \3--account name
    let matchArray = [];
    let guildMembersInfo = [];
    while ((matchArray = regex.exec(guildPageBody))!==null){
        guildMembersInfo.push({
            'playerID' : matchArray[1],
            'isActive' : !(matchArray[2] ==='disabled'),
            'playerName' : entities.decodeHTML(matchArray[3])
        });
    }
    return guildMembersInfo;
}

function fetchPlayerInfo(playerID) {
    return request('http://the-tale.org/game/api/info?api_version=1.3&api_client=guildstat-0.1&account=' + playerID)
        .then(function([ , body]) {
            const parsedJson = JSON.parse(body);
            if (parsedJson.status === 'error')
            {
                return null;
            }
            const result = {
                'playerID' : Number(playerID),
                'heroName' : parsedJson.data.account.hero.base.name,
                'race'     : Number(parsedJson.data.account.hero.base.race),
                'money'    : Number(parsedJson.data.account.hero.base.money),
                'level'    : Number(parsedJson.data.account.hero.base.level),
                'might'    : Number(parsedJson.data.account.hero.might.value),
                'isSubscribed' : Boolean(parsedJson.data.account.hero.permissions.can_repair_building)
            };
            return result;
        });
}

function zipWithIdx(promise, idx) {
    return new Promise(function(resolve, reject) {
        promise.then(data => {
            resolve([data, idx]);
        }, function(err) {
            reject([err, idx]);
        });
    });
}

function promiseMap(source, mapping, concurrency) {
    let running = 0;
    let idxToRun = 0;
    const l = source.length;
    let result = new Array(l);
    return new Promise(function(resolve, reject) {
        function onFulfilled(arr) {
            const data = arr[0];
            const idx = arr[1];
            result[idx] = data;
            running--;
            if (idxToRun < l) {
                zipWithIdx(mapping(source[idxToRun]), idxToRun++).then(onFulfilled, onRejected);
                running++;
            } else {
                if (running === 0) {
                    resolve(result);
                }
            }
        }
        function onRejected(arr) {
            const err = arr[0];
            reject(err);
        }
        while (running <= concurrency && idxToRun < l) {
            zipWithIdx(mapping(source[idxToRun]), idxToRun++).then(onFulfilled, onRejected);
            running++;
        }
    });
}