const getDb = require('../db.js');
const express = require('express');
const router = express.Router();
const error = require('debug')('api');

router.get('/guilds/:guildID', function(req, res, next) {
    getDb().then(db => {
        return db.collection('guilds').findOne({guildID: Number(req.params.guildID)}, {fields: {_id: 0, lastUpdated: 0}});
    }).then(guild => {
        res.json(guild);
    }).catch(err => {
        error(err);
        next();
    });
});

router.get('/guilds/:guildID/members', function(req, res, next) {
    getDb().then(db => {
        return db.collection('guilds').findOne({guildID: Number(req.params.guildID)}, {fields: {_id: 0, members: 1}});
    }).then(guild => {
        res.json(guild.members);
    }).catch(err => {
        error(err);
        next();
    });
});

router.get('/guilds', function(req, res) {
    getDb().then(db => {
        return db.collection('guilds').find().project({_id: 0, members: 0, lastUpdated: 0}).toArray();
    }).then(guilds => {
        res.json(guilds);
    }).catch(err => {
        error(err);
    });
});

module.exports = router;