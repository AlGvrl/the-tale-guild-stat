let MongoClient = require('mongodb').MongoClient;
let cfg = require('./cfg.js');
let db = null;
module.exports = function() {
    if (db) {
        return Promise.resolve(db);
    } else {
        return MongoClient.connect(cfg.db.URL).then((connection) => {
            db = connection;
            return db;
        });
    }
}; 