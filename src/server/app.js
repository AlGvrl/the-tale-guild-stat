const express = require('express');
const serveStatic = require('serve-static');
const morgan = require('morgan');
const api = require('./routes/api.js');

let app = express();
app.use(morgan('combined'));
app.use('/', serveStatic('public/'));
app.use('/api', api);

//catch 404?

module.exports = app;