const app = require('./app.js');
const cfg = require('./cfg.js');
const updater = require('./updater.js');
const debug = require('debug')('ttgs');

app.set('port', cfg.port);

let server = app.listen(app.get('port'), function() {
    debug('Express server listening on port ' + server.address().port);
});

updater.updateAllGuilds();
setInterval(updater.updateAllGuilds, cfg.prefUpdateTime);