exports.db = {};
exports.db.URL = process.env.MONGO_URL || 'mongodb://localhost:27017/ttgs';
exports.maxGuildID = 150;
exports.minUpdateTime = 11 * 60 * 60 * 1000;
exports.prefUpdateTime = 12 * 60 * 60 * 1000;
exports.port=process.env.PORT||3000;