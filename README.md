# The Tale Guild Stat
#### https://ttgs.herokuapp.com/

Might take a while to wake up on the first request. Should be nearly instant on consecutive ones.

### About

Collects info about players and guilds of one MMO[ZPG](https://en.wikipedia.org/wiki/Zero-player_game) (closer to Progress Quest than to Game of Life) [game](http://the-tale.org/) and neatly (neater than the original) presents it on a single page.

Also serves as a polygon for trying out new techs when I am curious about them.

Current version has React+Redux on the front-end and Express on the back-end. Both are a bit of an overkill. Fetched/parsed data are stored in MongoDB.